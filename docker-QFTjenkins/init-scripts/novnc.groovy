#! groovy
def NOVNCcmd = [ "/opt/websockify/run",
            "-D",
            "--web /opt/noVNC-1.0.0/",
            "--token-plugin jenkinsApiQuery.JenkinsNodeToken",
            "--token-source http://localhost:8080",
            "6080" ].join(" ")

if (System.getenv()["START_NOVNC"] && !(System.getenv()["START_NOVNC"] ==~/(no|NO|false|FALSE)/)) {
    println "starting novnc/websockify..."
    NOVNCcmd.execute();
}
