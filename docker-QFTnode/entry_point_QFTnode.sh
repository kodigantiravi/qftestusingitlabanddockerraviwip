#!/bin/bash
#
# IMPORTANT: Change this file only in directory StandaloneDebug!

source /opt/bin/functions.sh

export GEOMETRY="$SCREEN_WIDTH""x""$SCREEN_HEIGHT""x""$SCREEN_DEPTH"

function shutdown {
  kill -s SIGTERM $NODE_PID
  wait $NODE_PID
}

if [ ! -z $VNC_NO_PASSWORD ]; then
    echo "starting VNC server without password authentication"
    X11VNC_OPTS=
else
    X11VNC_OPTS=-usepw
fi

rm -f /tmp/.X*lock

SERVERNUM=$(get_server_num)

if [ -n "${JENKINS_MASTER}" ]; then
    MASTER_CONFIG=("-master" "${JENKINS_MASTER}");
fi

read -r < /run/secrets/jenkins-swarm

DISPLAY=$DISPLAY JSWARM_PW=$REPLY \
  xvfb-run -n $SERVERNUM --server-args="-screen 0 $GEOMETRY -ac +extension RANDR" \
  java -jar /usr/share/jenkins/swarm-client.jar -name QFTnode \
  -username swarm -passwordEnvVariable JSWARM_PW \
  ${MASTER_CONFIG[@]} -executors 1 &
NODE_PID=$!

unset ${REPLY}

trap shutdown SIGTERM SIGINT
for i in $(seq 1 10)
do
  xdpyinfo -display $DISPLAY >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    break
  fi
  echo Waiting xvfb...
  sleep 0.5
done

fluxbox -display $DISPLAY &

x11vnc $X11VNC_OPTS -forever -shared -rfbport 5900 -display $DISPLAY &

wait $NODE_PID
