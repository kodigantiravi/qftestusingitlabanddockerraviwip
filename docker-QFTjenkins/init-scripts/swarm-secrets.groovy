#!groovy
 
import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule
import org.apache.commons.lang.RandomStringUtils
 
def instance = Jenkins.getInstance()
def hudsonRealm = new HudsonPrivateSecurityRealm(false)


def user = "admin"
pass = new File("/run/secrets/jenkins-admin").text.trim()
hudsonRealm.createAccount(user, pass)

user = "swarm"
pass = new File("/run/secrets/jenkins-swarm").text.trim()
hudsonRealm.createAccount(user, pass)


instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()

Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)
