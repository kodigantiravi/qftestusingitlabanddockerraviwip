param(
  [string]$FileName
)

echo "------------------------- Starting Test Execution -------------------------"
powershell -Command mkdir HTMLReports
powershell -Command qftest -batch -compact -runlog -report HTMLReports $FileName
#powershell -Command qftest -batch "BrowserTestLaunchFirefox.qft"
#echo "------------------------- Completed Test Execution -------------------------"