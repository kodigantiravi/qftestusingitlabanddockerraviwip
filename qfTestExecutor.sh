#!/bin/bash

if [ "$1" == "" ]
then
    echo Usage: qfbatch Testsuite
    echo "No Parameter"
    exit -1
else
    suite=$1
    echo Usage: QFTEST EXECUTION
    qftest -batch -report.junit reports_junit -report.html reports_html $suite
    #ExecuteQFTest suite
fi

rc=$?
echo MESSAGE $rc